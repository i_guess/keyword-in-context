package kwicTest;

import java.util.Arrays;
import java.util.List;

import kwic.Alphabetizer;

import org.junit.Assert;
import org.junit.Test;

public class AlphabetizerTest {

	@Test
	public void sortTest() {
		
		Alphabetizer alph = new Alphabetizer();
		List<String> listToSort = Arrays.asList("bb", "b", "a", "a", "z", "c");
		alph.sortStrings(listToSort);
		Assert.assertEquals(listToSort, 
				Arrays.asList("a", "a", "b", "bb", "c", "z"));
	}
}