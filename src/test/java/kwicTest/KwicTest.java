package kwicTest;

import kwic.Kwic;

import org.junit.Assert;
import org.junit.Test;

public class KwicTest {

	@Test
	public void kwicIndexTest() {
		String[] testStrings = { "messapian peewee bunton",
				"loathsome nonpositivistic noteworthily orthodontia palliative" };
		Kwic kwic = new Kwic(testStrings);

		String[] expectedResults = {
				"bunton messapian peewee",
				"loathsome nonpositivistic noteworthily orthodontia palliative",
				"messapian peewee bunton",
				"nonpositivistic noteworthily orthodontia palliative loathsome",
				"noteworthily orthodontia palliative loathsome nonpositivistic",
				"orthodontia palliative loathsome nonpositivistic noteworthily",
				"palliative loathsome nonpositivistic noteworthily orthodontia",
				"peewee bunton messapian" };
		Assert.assertArrayEquals(expectedResults, kwic.index().toArray());
	}

	@Test
	public void kwicSearchtest() {
		String[] testStrings = { "messapian peewee bunton",
				"loathsome nonpositivistic noteworthily orthodontia palliative" };
		Kwic kwic = new Kwic(testStrings);

		String[] expectedResults = { "peewee bunton messapian" };
		
		Assert.assertArrayEquals(expectedResults, kwic.search("peewee").toArray());
	}
}
