package kwicTest;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import kwic.InputReader;

import org.junit.Assert;
import org.junit.Test;

public class InputReaderTest {
	
	@Test
	public void inputTest() {
		String testString = "aa bb cc" + System.lineSeparator() + "a2 b2 c2";
		InputStream stream = new ByteArrayInputStream(testString.getBytes(StandardCharsets.UTF_8));
		InputReader reader = new InputReader();
		List<List<String>> actualLines = reader.readLinesByWords(stream);
		Assert.assertArrayEquals(new String[]{"aa", "bb", "cc"}, actualLines.get(0).toArray());
		Assert.assertArrayEquals(new String[]{"a2", "b2", "c2"}, actualLines.get(1).toArray());
		Assert.assertEquals(2, actualLines.size());
	}
	
	@Test
	public void inputTestAdditionalLineSeparator() {
		String testString = "aa bb cc" + System.lineSeparator() + "a2 b2 c2" + System.lineSeparator();
		InputStream stream = new ByteArrayInputStream(testString.getBytes(StandardCharsets.UTF_8));
		InputReader reader = new InputReader();
		List<List<String>> actualLines = reader.readLinesByWords(stream);
		Assert.assertEquals(2, actualLines.size());
	}
}
