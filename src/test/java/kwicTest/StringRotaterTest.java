package kwicTest;

import kwic.StringRotater;

import org.junit.Assert;
import org.junit.Test;

public class StringRotaterTest {

	@Test
	public void rotateTest() {
		StringRotater sr = new StringRotater();
		sr.fitString("hello world test");
		Assert.assertEquals("world test hello", sr.rotate(1));
		Assert.assertEquals("test hello world", sr.rotate(2));
		Assert.assertEquals("hello world test", sr.rotate(3));
	}
}
