package kwicTest;

import kwic.Line;

import org.junit.Assert;
import org.junit.Test;

public class LineTest {

	@Test
	public void lineTest(){
		Line line = new Line("Hello test world Hello");
		
		Integer[] expectedResults = {0, 3};
		
		Assert.assertArrayEquals(expectedResults, line.search("Hello").toArray());
	}
}
