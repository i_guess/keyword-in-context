package kwic;

/**
 * This object takes arrays of strings and rotates them by word
 *
 */
public class StringRotater {

	private String[] strings;
	
	public StringRotater(){
		
	}
	
	public StringRotater(String rotateString){
		strings = rotateString.split(" ");
	}
	
	public StringRotater(String[] strings){
		this.strings = strings;
	}
	
	public void fitString(String rotateString){
		strings = rotateString.split(" ");
	}
	
	public String rotate(int rotateBy){
		StringBuilder builder = new StringBuilder();
		rotateBy = rotateBy % strings.length;
		for(int i = rotateBy; i < strings.length; i++){
			if(i != rotateBy){
				builder.append(' ');
			}
			builder.append(strings[i]);
		}
		for(int i = 0; i < rotateBy; i++){
			builder.append(' ');
			builder.append(strings[i]);
		}
		return builder.toString();
	}
	
}
