package kwic;

import java.io.InputStream;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class InputReader {
	
	public InputReader(){
	}
	
	/**
	 * @param inputStream Stream to read from. System.in may be passed.
	 * @return	List of lines, each line contains list of words in order of reading.
	 */
	public List<List<String>> readLinesByWords(InputStream inputStream){
	    Scanner s = new Scanner(inputStream);
	    ArrayList<List<String>> result = new ArrayList<List<String>>();
	    
	    while (s.hasNextLine()) {
	    	List<String> lineByWords = Arrays.asList(s.nextLine().split(" "));
	    	result.add(lineByWords);
	    }
	    
	    s.close();
	    return result;
	}

}
