package kwic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class KwicDemo {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter the name of file to test: ");
		File testFile = new File(scanner.nextLine());
		InputReader inputReader = new InputReader();
		try {
			List<List<String>> strings = inputReader
					.readLinesByWords(new FileInputStream(testFile));
			Kwic kwic = new Kwic(strings);

			System.out.println("\nIndex of all rotations");

			for (String s : kwic.index()) {
				System.out.println(s);
			}

			System.out.println("\nPlease enter term to search for: ");
			String searchTerm = scanner.nextLine();
			
			System.out.println("\nIndex of search results for "+ searchTerm+": \n");

			for (String s : kwic.search(searchTerm)) {
				System.out.println(s);
			}

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		scanner.close();

	}

}
