package kwic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Line {
	private String lineString;
	private int lengthInWords;
	private HashMap<String, ArrayList<Integer>> indexes;
	
	public Line(){
		lineString = "";
	}
	
	public Line(String lineString){
		this.lineString = lineString;
		fillIndexes(lineString.split(" "));
	}
	
	public Line(List<String> strings){
		this((String[])strings.toArray());
	}
	
	public Line(String[] strings){
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < strings.length; i++){
			if(i > 0){
				builder.append(" ");
			}
			builder.append(strings[i]);
		}
		lineString = builder.toString();
		fillIndexes(strings);
	}
	
	private void fillIndexes(String[] strings){
		lengthInWords = strings.length;
		indexes = new HashMap<String, ArrayList<Integer>>();
		for(int i = 0; i < strings.length; i++){
			if(indexes.containsKey(strings[i])){
				indexes.get(strings[i]).add(i);
			}
			else{
				ArrayList<Integer> arr = new ArrayList<Integer>();
				arr.add(i);
				indexes.put(strings[i], arr);
			}
		}
	}
	
	public String getLineString(){
		return lineString;
	}
	
	public int getLengthInWords(){
		return lengthInWords;
	}
	
	public ArrayList<Integer> search(String searchTerm){
		return indexes.get(searchTerm);
	}
}
