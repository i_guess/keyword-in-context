package kwic;

import java.util.Collections;
import java.util.List;

public class Alphabetizer {

	public Alphabetizer() {
	}

	/**
	 * @param List
	 *            of strings to be sorted alphabetically.
	 */
	public void sortStrings(List<String> listToSort) {
		Collections.sort(listToSort);
	}
}
