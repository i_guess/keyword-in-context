package kwic;

import java.util.ArrayList;
import java.util.List;

public class Kwic {

	private Line[] lines;
	private Alphabetizer alphabetizer;

	public Kwic() {
		alphabetizer = new Alphabetizer();
	}

	/**
	 * Constructor to make a Kwic based on a each input line being a string
	 * 
	 * @param strings
	 *            parameter to build the kwic index around
	 */
	public Kwic(String[] strings) {
		alphabetizer = new Alphabetizer();
		lines = new Line[strings.length];
		for (int i = 0; i < lines.length; i++) {
			lines[i] = new Line(strings[i]);
		}
	}

	public Kwic(List<List<String>> strings) {
		this();
		lines = new Line[strings.size()];
		for (int i = 0; i < lines.length; i++) {
			lines[i] = new Line(strings.get(i));
		}
	}

	/**
	 * create an index of all possible string rotations
	 * 
	 * @return the alphabetized index
	 */
	public List<String> index() {

		// Create index with string rotater based on line strings
		List<String> index = new ArrayList<String>();
		StringRotater sr = new StringRotater();
		for (Line line : lines) {
			sr.fitString(line.getLineString());
			for (int i = 0; i < line.getLengthInWords(); i++) {
				index.add(sr.rotate(i));
			}
		}

		alphabetizer.sortStrings(index);
		return index;
	}

	/**
	 * This method is for searching for keywords in the Kwic and it returns all
	 * results with match as the first word
	 * 
	 * 
	 * @param searchTerm
	 *            the term to search for
	 * @return alphabetized search index
	 */
	public List<String> search(String searchTerm) {
		List<String> index = new ArrayList<String>();
		StringRotater sr = new StringRotater();
		for (Line line : lines) {
			List<Integer> indexes = line.search(searchTerm);
			if (indexes == null) {
				continue;
			}

			sr.fitString(line.getLineString());
			for (Integer i : indexes) {
				index.add(sr.rotate(i));
			}
		}

		alphabetizer.sortStrings(index);
		return index;
	}
}
